#%% Convert all PPTs in a folder to PDF format

# Purpose: Converts all PowerPoint PPTs in a folder and all subfolders to Adobe PDF and deletes the existing files

# Author:  Twan Houwers

# Usage: Run code in Spyder/Pycharm etc
#   - input_folder_path: the parent directory all PowerPoint files 

#%% Import libraries
import os
import comtypes.client

#Define input folder (change the folder here to your folder)
input_folder_path = 'C:\\Users\\thouwers\\\OneDrive - Deloitte (O365D)\\Documents\\testfolder'
 
#%% Loop over all sub directories
i = 0 
for folder, subfolders, files in os.walk(input_folder_path):
    for file in files:
        if file.endswith('.pptx'):
            if folder == os.path.join(input_folder_path, '2. Puzzles\\Puzzle F4\\Puzzle'):
                print('Skipped Puzzle F4 for conversion to PDF')
                continue
            # set input and output folder 
            inputpath = os.path.join(folder,file)
            
            ## get file name
            file_name = os.path.splitext(file)[0]

            ## Create output pdf
            outputpath = os.path.join(folder,file_name + ".pdf")    
            
            ## Create powerpoint application object
            powerpoint = comtypes.client.CreateObject("Powerpoint.Application")
            
            ## Set visibility to minimize
            powerpoint.Visible = 1
            
            ##Open the powerpoint slides
            slides = powerpoint.Presentations.Open(inputpath)
            
            ## Save as PDF (formatType = 32)
            slides.SaveAs(outputpath, 32)
            
            #Close the slide deck
            slides.Close()
            
            ##Delete PPT file
            os.remove(inputpath)
            
            ##counter and print progress 
            i = i + 1
            print("converted & deleted " + file + " as file " + str(i))
#%% END     

            
    