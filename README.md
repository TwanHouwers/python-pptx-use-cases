# README #

### What is this repository for? ###
Python files to interact with powerpoint files

1. Script to save all PPTX files in repository and sub directories to PDF and delte all PPTX files
2. Script to replace all footers in all slidemasters & underlying slide layouts from Deloitte 201x or 201x Deloitte to 2020 in specific folder and all subfolders

#### Install ####
Install packages before running the script
```install
pip install python-pptx
pip install pyinstaller
pip install re
pip install os
```
* Set an input path and define input variables in the indiviudal script
* EXE file is downloadable at https://cognitive.deloitte.nl/PPTX-footerfix

### Keep in mind before running the script ###
* Definie the correct input variables
* Close all PPTX files before running the script
* Make sure you define settings such that you keep/delete the old files to your preference

### Contact ###

* Repo owner: Twan Houwers