#%% Adjust slide text in all powerpoint documents with a folder and its subdirectories
# Description:
#   - Changes the defined text within text elements and placeholders in all SLide Masters and all underlying slide layouts
#   - For example "Deloitte 2019" will be changed to "Deloitte 2020"
#   - Also captures the terms and conditions slide format

# Author:  Twan Houwers

# Usage: Run code in any IDE:
#   - input_folder_path: the parent directory of all PowerPoint files
#   - output_folder_path: the output folder where all files should be stored (equal to input_folder_path for replacement of files)
#   - old_footer_text_options: array of regex text elements that powerpoint text element should contain to be altered
#   - text_to_replace: the text (yearnumber's) that will be replaced in powerpoint text elements (regex possible)
#   - new_text: the new text (yearnumber) to be shown in powerpoint presentation

# Installation: 
# 1. pip install python-pptx
# 2. pip install os
# 3. pip install re

#%% 
## Import libraries
import os
from pptx import Presentation
import re
#%%
print('Script started')
filepath = os.path.dirname(os.path.abspath(__file__))
os.chdir(filepath)

## Define old text footer currently in presentation and text to be changed to
old_footer_text_options = ['201[0-9]\s+Deloitte','Deloitte\s+201[0-9]'] # All combinations of Deloitte and 2010-2019
text_to_replace = '201[0-9]' #2010-2019
new_text = '2020'

#%% Define function that replace text in text-objects which contain element of old_footer_text_options
def replace_text_in_objects(objects):
    #loop over all objects
    for obj in objects:
        # check if there is a text_frame
        if obj.has_text_frame:
            # loop over all paragraph of a text_frame
            for paragraph in obj.text_frame.paragraphs:
                # loop over all runs in a paragraph
                for run in paragraph.runs:
                    # replace the year number in the text if one of footer terms is present
                    if any(re.findall(x,run.text) for x in old_footer_text_options):
                        run.text = re.sub(text_to_replace ,new_text, run.text).encode('utf-8')
              
### Define function that replaces text in shapes & placeholders for a defined slidemaster or slide_layout
def shapes_placeholders_replace(layout):
    #replace all text in shape objects
    replace_text_in_objects(layout.shapes)
    #replace all text in placeholders
    replace_text_in_objects(layout.placeholders)

#%% Loop over all sub directories in folder and replace text in every .pptx file
i = 0 
for folder, subfolders, files in os.walk(filepath):
    for file in files:
        if file.endswith('.pptx'):
            i = i + 1 
            # set input and output folder 
            inputpath = os.path.join(folder,file)
            
            # open presentation
            prs = Presentation(pptx=inputpath)
            
            #open all slide masters
            slide_masters = prs.slide_masters
            
            #loop over all slide masters
            for slidemaster in slide_masters:
                # replace text in slidemaster
                shapes_placeholders_replace(slidemaster)
                
                #loop over all layouts in slidemaster
                for layout in slidemaster.slide_layouts:
                    # replace text in slide layout
                    shapes_placeholders_replace(layout)
           
            ## Save presentation after all changes with added text
            save_file = file.replace('.pptx','_' + new_text + '.pptx')
            prs.save(os.path.join(folder,save_file))
            print(str(i) + ' presentations converted')
#%% END     